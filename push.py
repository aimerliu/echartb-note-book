'''
Author: your name
Date: 2021-11-03 15:14:24
LastEditTime: 2021-11-11 18:25:59
LastEditors: your name
Description: 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
FilePath: \middleground\push.py
'''
import os
import sys
import datetime
import winreg

if __name__  == '__main__':

    default = '刘娅清'

    if "GIT_NAME" in os.environ:
        username = os.getenv('GIT_NAME')
    else:
        os.putenv('GIT_NAME', default)
        username = default

    if len(sys.argv) > 1:
        desc = sys.argv[1]
    else:
        curr = datetime.datetime.now()
        desc = f"{username}=>{curr.hour}:{curr.minute}:{curr.second}-update"

    os.system('git add ./')
    os.system(f'git commit -m "{desc}"')
    os.system('git pull')
    os.system('git push')